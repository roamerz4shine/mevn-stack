var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var Book = require('../models/Book');
var passport = require('passport');
require('../config/passport')(passport);

router.get('/', passport.authenticate('jwt', { session: false, failWithError: true }), (req, res, next) => {
  Book.find((err, products) => {
    if (err) return next(err);
    res.json(products);
  });
}, (err, req, res, next) => {
  return res.status(401).json({ success: false, msg: 'Unauthorized.' });
});

router.get('/:id', passport.authenticate('jwt', { session: false, failWithError: true }), (req, res, next) => {
  Book.findById(req.params.id, (err, post) => {
    if (err) return next(err);
    res.json(post);
  });
}, (err, req, res, next) => {
  return res.status(401).json({ success: false, msg: 'Unauthorized.' });
});

router.post('/', passport.authenticate('jwt', { session: false, failWithError: true }), (req, res, next) => {
  Book.create(req.body, (err, post) => {
    if (err) return next(err);
    res.json(post);
  });
}, (err, req, res, next) => {
  return res.status(401).json({ success: false, msg: 'Unauthorized.' });
});

router.put('/:id', passport.authenticate('jwt', { session: false, failWithError: true }), (req, res, next) => {
  Book.findByIdAndUpdate(req.params.id, req.body, (err, post) => {
    if (err) return next(err);
    res.json(post);
  });
}, (err, req, res, next) => {
  return res.status(401).json({ success: false, msg: 'Unauthorized.' });
});

router.delete('/:id', passport.authenticate('jwt', { session: false, failwithError: true }), (req, res, next) => {
  Book.findByIdAndRemove(req.params.id, req.body, (err, post) => {
    if (err) return next(err);
    res.json(post);
  });
}, (err, req, res, next) => {
  return res.status(401).json({ success: false, msg: 'Unauthorized.' });
});

module.exports = router;
